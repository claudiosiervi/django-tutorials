# Welcome to the Django

## Setup with VirtualEnv

* install pip3  
 sudo apt-get install python3-pip  

* install virtualenv with python3  
pip3 install virtualenv  

* create a virtualenv with python3  
virtualenv -p python3 vm_django  

* inatialize virtualen  
source  vm_django/bin/activate  

* install django and other packages listed into requirements file  
pip install -m requirements.txt  

* init a new django project (the single dot after the mysite is needed to create the file manage.py inside the project folder)  
django-admin startproject mysite .  

* create a database (after configure the database in the settings.py file)  
python3 manage.py migrate  

* running the server (in the same level of the manage.py file)
python3 manage.py runserver  

* creating application - blog  
python manage.py startapp blog  

* create table for the model (after define the model in the blog/models file)  
python3 manage.py makemigrations blog  

* create a super user (after import the Post model to the Admin)  
python manage.py createsuperuser  
